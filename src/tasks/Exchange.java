package tasks;

import data.Settings;
import org.rspeer.runetek.adapter.scene.Npc;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.component.Dialog;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.movement.position.Area;
import org.rspeer.runetek.api.scene.Npcs;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.script.task.Task;

public class Exchange extends Task {

    private Npc phials;
    private Area outside;

    @Override
    public boolean validate() {
        outside = Area.rectangular(2954, 3223, 2948, 3211);
        phials = Npcs.getNearest("Phials");
        return !Settings.hasExchanged && phials != null && !Inventory.contains(1761);
    }

    @Override
    public int execute() {
        phials = Npcs.getNearest("Phials");
        if (outside.contains(Players.getLocal())) {
            if (phials != null) {
               // HyperTabs.status = "Selecting clay";
                Inventory.getFirst("Soft clay").interact("Use");
                Time.sleepUntil(Inventory::isItemSelected, 1200);
              //  HyperTabs.status = "Using clay on Phials";
                phials.interact("Use");
                Time.sleepUntil(Dialog::isProcessing, 5000);
               // HyperTabs.status = "Exchanging";
                Dialog.process(2);
                Settings.hasExchanged = true;
            }
        }
        return 600;
    }
}
