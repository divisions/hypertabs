package tasks;

import data.Settings;
import org.rspeer.runetek.adapter.scene.SceneObject;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.commons.math.Random;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.scene.SceneObjects;
import org.rspeer.script.task.Task;

public class Exit extends Task {

    private SceneObject portal;

    @Override
    public boolean validate() {
        return !Inventory.contains(1761) && !Settings.hasExchanged;
    }

    @Override
    public int execute() {
        portal = SceneObjects.getNearest("Portal");

        if (portal != null) {
            //HyperTabs.status = "Exiting house";
            if (portal.interact("Enter")) {
                Time.sleep(Random.mid(600, 1800));
            }
        }
        return 600;
    }
}
